from django.db.models import Q
from rest_framework import status, viewsets
from rest_framework.response import Response
from ..models import FeatureToggle


class FeaturefulViewSet(viewsets.ModelViewSet):
    '''
    Featureful model utilities for django viewsets
    '''
    feature_name = "Core"

    def is_enabled(self, request):
        '''
        Validates if a feature is enabled before use
        '''
        if (FeatureToggle.objects.filter(Q(name=self.feature_name, enabled=True)).count() == 0):
            return Response(
                {
                    'type': 'Feature',
                    'title': self.feature_name,
                    'details': 'Offline',
                    'instance': request.path
                },
                status=status.HTTP_403_FORBIDDEN
            )
