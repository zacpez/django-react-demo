import uuid
from django.contrib.auth.models import User, Group
from django.http import QueryDict
from rest_framework import permissions, viewsets
from rest_framework.response import Response
from .serializers import UserSerializer, GroupSerializer, FeatureToggleSerializer, ChatSerializer
from .models import FeatureToggle, Chat
from backend.essential.base import FeaturefulViewSet


class UserViewSet(viewsets.ModelViewSet):
    """
    Users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]


class GroupViewSet(viewsets.ModelViewSet):
    """
    User Groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [permissions.IsAuthenticated]


class FeatureViewSet(viewsets.ModelViewSet):
    """
    FeatureToggles to be immediately change functionality.
    """
    queryset = FeatureToggle.objects.all()
    serializer_class = FeatureToggleSerializer
    permission_classes = [permissions.IsAuthenticated]


class ChatViewSet(FeaturefulViewSet):
    """
    Chat messages in threads.
    """
    queryset = Chat.objects.all()
    serializer_class = ChatSerializer
    permission_classes = [permissions.IsAuthenticated]
    feature_name = "Chat"

    def create(self, request, *args, **kwargs):
        if (response := self.is_enabled(request)):
            return response

        if isinstance(request.data, QueryDict):
            request.data._mutable = True

        message_count = Chat.objects.filter(message_id=request.data['thread_id']).count()
        message_id = str(uuid.uuid4()) if message_count > 0 else request.data['thread_id']

        request.data['message_id'] = message_id
        request.data['user_id'] = request.user.id

        Chat.objects.create(**request.data)
        return Response(request.data)
