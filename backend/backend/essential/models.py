import uuid
from django.utils.translation import gettext_lazy as _
from django.db import models
from django.contrib.auth.models import User


# Base Entity
class BaseEntity(models.Model):
    created = models.DateTimeField(
        _("Created At"),
        auto_now_add=True,
        null=False
    )
    updated = models.DateTimeField(
        _("Updated At"),
        auto_now=True,
        null=False
    )

    class Meta:
        abstract = True


# Basic Feature Flag that something might hook into
class FeatureToggle(BaseEntity):
    enabled = models.BooleanField(
        _("Is feature enabled"),
        default=False,
        help_text=_("Is feature by Id enabled"),
    )

    name = models.TextField(
        _("Feature name"),
        default="",
        null=False,
        blank=False,
        help_text=_("The name of the feature being deliverd when enabled"),
    )

    description = models.TextField(
        _("Feature description"),
        null=False,
        blank=True,
        help_text=_("Expanded details of the feature being deliverd when enabled"),
    )

    parent = models.ForeignKey(
        "self",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        help_text=_("Expanded details of the feature being deliverd when enabled"),
    )


class Chat(BaseEntity):
    message_id = models.UUIDField(
        _("Message UUID"),
        primary_key=True,
        editable=False,
        null=False,
        blank=False,
        help_text=_("Unique message ID across all messages"),
    )

    thread_id = models.UUIDField(
        _("Thread UUID"),
        default=uuid.uuid4,
        null=False,
        blank=True,
        help_text=_("Unique thread ID across all threads, matches message ID on first message"),
    )

    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
    )

    message = models.TextField(
        _("Message body"),
        null=False,
        blank=True,
        help_text=_("Message sent by user"),
    )
