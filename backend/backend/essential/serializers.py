from django.contrib.auth.models import User, Group
from rest_framework import serializers
from .models import FeatureToggle, Chat


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']


class ProblemDetailsSerializer(serializers.Serializer):
    type = serializers.CharField()
    title = serializers.CharField()
    detail = serializers.CharField()
    instance = serializers.CharField()

    class Meta:
        fields = ('type', 'title', 'detail', 'instance')

    def __init__(self, instance=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if instance:
            self.instance = {
                'type': instance.type,
                'title': instance.code,
                'detail': instance.detail,
                'instance': instance.instance
            }


class FeatureToggleSerializer(serializers.ModelSerializer):
    class Meta:
        model = FeatureToggle
        fields = ['id', 'enabled', 'name', 'description', 'parent']


class ChatSerializer(serializers.ModelSerializer):
    class Meta:
        model = Chat
        fields = ['message_id', 'thread_id', 'message', 'user_id']
