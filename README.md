# Django & React Demo

This demo consist of a very simple implementation of Django and React application.

## Getting started

Prerequisites:

* python 3.10+
* node 18+
* postgresql
* just (optional)

## Dev Commands

Sometimes you just wanna dev for from your host machine.

```bash
# Doing backend operations, change the directory context
cd backend

# Attach to your venv
./venv/Scripts/activate # or which ever way it is setup for you

# Run
python manage.py runserver

# Make DB Migrations
python manage.py makemigrations backend

# Run outdated migrations
python manage.py migrate

# Rollback 
python manage.py migrate 00XX

# Rollback all 
python manage.py migrate zero
``

## Technical review

There is a slim setup to run this with docker compose. One pattern here is required to launch the python application after the DB is only and accepting connections. This is relavely a liveness probe to orchestrate standing up the environments.

## Next steps

Main pillars of future development and some simple todos.

1. Docker compose clean up
2. Create DEV TEST PROD enviroment sets
3. Disable DEBUG flag in TEST+ builds
4. Add allowed hosts values in environment variables
5. Add environment secret values to environment variables
6. Add DB credential values to environment variables
7. Add some basic testing ;)
