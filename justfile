#!/usr/bin/env -S just --justfile
# ^ A shebang isn't required, but allows a justfile to be executed
set shell := ["powershell.exe", "-c"]

venv:
    ./venv/Scripts/activate

mm:
    python backend/manage.py makemigrations backend

mu:
    python backend/manage.py migrate

run:
    python backend/manage.py runserver